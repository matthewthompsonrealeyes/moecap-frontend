// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  auth0: {
    clientId: 'bVTsaerJQZPAV4W21DJOSFP9XSEOHq4N',
    domain: 'realeyes.auth0.com',
    responseType: 'token',
    audience: 'http://moecap-login',
    loginRedirectUri: 'http://localhost:4200/login-callback',
    logoutReturnToUri: 'http://localhost:4200/login',
    realm: 'Username-Password-Authentication',
    tokenName: 'authToken',
    errors: {
      accessDenied: {
        code: 'access_denied',
        message: 'Your credentials did not match. Please check your email and password and try again.'
      },
      default: {
        message: 'There was a problem logging you in. Please contact your administrator.'
      }
    }
  },
  api: {
    baseUrl: '',
    capture: {
      createUrl: '',
      deleteUrl: ''
    },
    checkFiles: {
      createUrl: ''
    },
    convertToHar: {
      createUrl: ''
    }
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
