import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '../../../node_modules/@angular/forms';
import { MatButtonModule, MatCardModule, MatDividerModule, MatFormFieldModule, MatInputModule } from '../../../node_modules/@angular/material';
import { LoginComponent } from './login.component';
import { LoginCallbackComponent } from './login-callback/login-callback.component';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDividerModule,
    ReactiveFormsModule
  ],
  declarations: [
    LoginComponent,
    LoginCallbackComponent
  ]
})
export class LoginModule { }
