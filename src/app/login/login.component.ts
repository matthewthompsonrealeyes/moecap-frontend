import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '../../../node_modules/@angular/forms';
import { Subject } from '../../../node_modules/rxjs';
import { Auth0Service } from '../core/authentication/auth0.service';
import { AuthenticationCredentials } from '../models/AuthenticationCredentials';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private loginForm: FormGroup;
  private loginError$: Subject<string>;

  constructor(
    private auth0Service: Auth0Service,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.loginError$ = this.auth0Service.loginError$;

    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  login(): void {
    const authenticationCredentials: AuthenticationCredentials = {
      email: this.loginForm.get('email').value,
      password: this.loginForm.get('password').value
    };

    this.auth0Service.login(authenticationCredentials);
  }
}
