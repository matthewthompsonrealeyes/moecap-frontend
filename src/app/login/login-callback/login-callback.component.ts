import { Component, OnInit } from '@angular/core';
import { Auth0Service } from '../../core/authentication/auth0.service';

@Component({
  selector: 'app-login-callback',
  templateUrl: './login-callback.component.html',
  styleUrls: ['./login-callback.component.scss']
})
export class LoginCallbackComponent implements OnInit {

  constructor(
    private auth0Service: Auth0Service
  ) { }

  ngOnInit() {
    this.auth0Service.handleLoginCallback();
  }

}
