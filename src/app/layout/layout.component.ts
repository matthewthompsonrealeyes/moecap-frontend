import { Component, OnInit } from '@angular/core';
import { Auth0Service } from '../core/authentication/auth0.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  constructor(
    private auth0Service: Auth0Service
  ) { }

  ngOnInit() {
  }

  logout() {
    this.auth0Service.logout();
  }
}
