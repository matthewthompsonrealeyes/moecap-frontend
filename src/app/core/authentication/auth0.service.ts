import { Injectable } from '@angular/core';
import { JwtHelperService } from '../../../../node_modules/@auth0/angular-jwt';
import * as auth0 from 'auth0-js';
import { environment } from '../../../environments/environment';
import { AuthenticationCredentials } from '../../models/AuthenticationCredentials';
import { Router } from '../../../../node_modules/@angular/router';
import { Subject } from '../../../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class Auth0Service {
  loginError$: Subject<string> = new Subject<string>();

  private auth0 = new auth0.WebAuth({
    clientID: environment.auth0.clientId,
    domain: environment.auth0.domain,
    responseType: environment.auth0.responseType,
    audience: environment.auth0.audience,
    redirectUri: environment.auth0.loginRedirectUri
  });

  constructor(
    private jwtHelper: JwtHelperService,
    private router: Router
  ) { }

  isAuthenticated(): boolean {
    const token = localStorage.getItem(environment.auth0.tokenName);

    return !this.jwtHelper.isTokenExpired(token);
  }

  login(credentials: AuthenticationCredentials): void {
    this.auth0.login({
      username: credentials.email,
      password: credentials.password,
      realm: environment.auth0.realm
    }, (err, result) => {
      if (err) {
        switch (err.error) {
          case environment.auth0.errors.accessDenied.code:
            this.loginError$.next(environment.auth0.errors.accessDenied.message);
            break;

          default:
            this.loginError$.next(environment.auth0.errors.default.message);
        }
      }
    });
  }

  handleLoginCallback() {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken) {
        window.location.hash = '';
        localStorage.setItem(environment.auth0.tokenName, authResult.accessToken);
        this.router.navigate(['/']);
      } else {
        this.router.navigate(['/login']);
      }
    });
  }

  logout() {
    localStorage.removeItem(environment.auth0.tokenName);

    this.auth0.logout({
      returnTo: environment.auth0.logoutReturnToUri,
      clientID: environment.auth0.clientId
    });
  }
}
