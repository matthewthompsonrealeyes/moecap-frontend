import { Injectable } from '@angular/core';
import { CanActivate, Router } from '../../../../node_modules/@angular/router';
import { Auth0Service } from './auth0.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private auth0Service: Auth0Service, private router: Router) { }

  canActivate(): boolean {
    if (!this.auth0Service.isAuthenticated()) {
      this.router.navigate(['/login']);

      return false;
    }

    return true;
  }

}
