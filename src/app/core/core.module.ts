import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Auth0Service } from './authentication/auth0.service';
import { AuthGuardService } from './authentication/auth-guard.service';
import { JwtModule } from '@auth0/angular-jwt';
import { tokenGetter } from '../consts/token-getter';
import { HttpClientModule } from '../../../node_modules/@angular/common/http';
import { RouterModule } from '../../../node_modules/@angular/router';
import { ApiService } from './api/api.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter
      }
    })
  ],
  providers: [
    Auth0Service,
    AuthGuardService,
    ApiService
  ]
})
export class CoreModule { }
