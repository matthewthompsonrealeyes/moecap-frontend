import { Injectable } from '@angular/core';
import { Observable } from '../../../../node_modules/rxjs';
import { environment } from '../../../environments/environment';
import { CreateCaptureRequest } from '../../models/api/capture/CreateCaptureRequest';
import { CreateCaptureResponse } from '../../models/api/capture/CreateCaptureResponse';
import { ApiService } from './api.service';
import { DeleteCaptureResponse } from '../../models/api/capture/DeleteCaptureResponse';

@Injectable({
  providedIn: 'root'
})
export class CaptureApiService {

  constructor(
    private apiService: ApiService
  ) { }

  createCapture(request: CreateCaptureRequest): Observable<CreateCaptureResponse> {
    return this.apiService.post<CreateCaptureResponse>(environment.api.capture.createUrl, request);
  }

  deleteCapture(captureId: string): Observable<DeleteCaptureResponse> {
    return this.apiService.delete<DeleteCaptureResponse>(environment.api.capture.deleteUrl + '/' + captureId);
  }
}
