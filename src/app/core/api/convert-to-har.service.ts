import { Injectable } from '@angular/core';
import { CreateConvertToHarRequest } from '../../models/api/convert-to-har/CreateConvertToHarRequest';
import { Observable } from '../../../../node_modules/rxjs';
import { ApiService } from './api.service';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConvertToHarService {

  constructor(
    private apiService: ApiService
  ) { }

  createConvertToHar(request: CreateConvertToHarRequest): Observable<any> {
    return this.apiService.post(environment.api.convertToHar.createUrl, request);
  }
}
