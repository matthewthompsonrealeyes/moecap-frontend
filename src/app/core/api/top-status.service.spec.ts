import { TestBed, inject } from '@angular/core/testing';

import { TopStatusService } from './top-status.service';

describe('TopStatusService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TopStatusService]
    });
  });

  it('should be created', inject([TopStatusService], (service: TopStatusService) => {
    expect(service).toBeTruthy();
  }));
});
