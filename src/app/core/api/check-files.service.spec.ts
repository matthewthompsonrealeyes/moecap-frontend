import { TestBed, inject } from '@angular/core/testing';

import { CheckFilesService } from './check-files.service';

describe('CheckFilesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CheckFilesService]
    });
  });

  it('should be created', inject([CheckFilesService], (service: CheckFilesService) => {
    expect(service).toBeTruthy();
  }));
});
