import { TestBed, inject } from '@angular/core/testing';

import { CaptureApiService } from './capture-api.service';

describe('CaptureApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CaptureApiService]
    });
  });

  it('should be created', inject([CaptureApiService], (service: CaptureApiService) => {
    expect(service).toBeTruthy();
  }));
});
