import { Injectable } from '@angular/core';
import { HttpClient } from '../../../../node_modules/@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiUrl: string;

  constructor(
    private http: HttpClient
  ) {
    this.apiUrl = environment.api.baseUrl;
  }

  get<T>(endpoint: string): any {
    return this.http.get<T>(this.apiUrl + endpoint);
  }

  post<T>(endpoint: string, body: any): any {
    return this.http.post<T>(this.apiUrl + endpoint, body);
  }

  put<T>(endpoint: string, body: any): any {
    return this.http.put<T>(this.apiUrl + endpoint, body);
  }

  delete<T>(endpoint: string): any {
    return this.http.delete<T>(this.apiUrl + endpoint);
  }
}
