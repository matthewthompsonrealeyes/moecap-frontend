import { TestBed, inject } from '@angular/core/testing';

import { ConvertToHarService } from './convert-to-har.service';

describe('ConvertToHarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConvertToHarService]
    });
  });

  it('should be created', inject([ConvertToHarService], (service: ConvertToHarService) => {
    expect(service).toBeTruthy();
  }));
});
