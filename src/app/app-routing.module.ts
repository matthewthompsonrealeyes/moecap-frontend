import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LayoutComponent } from './layout/layout.component';
import { AuthGuardService } from './core/authentication/auth-guard.service';
import { HomeComponent } from './home/home.component';
import { LoginCallbackComponent } from './login/login-callback/login-callback.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'login-callback', component: LoginCallbackComponent },
  { path: '', canActivate: [AuthGuardService], component: LayoutComponent, children: [
    { path: '', component: HomeComponent }
  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
