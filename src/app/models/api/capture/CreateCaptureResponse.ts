export interface CreateCaptureResponse {
  captureId: string;
}
